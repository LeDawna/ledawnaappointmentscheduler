namespace AppointmentScheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        AddressId = c.Int(nullable: false, identity: true),
                        Address1 = c.String(nullable: false),
                        Address2 = c.String(),
                        City = c.String(nullable: false),
                        State = c.String(nullable: false),
                        Zip = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.AddressId);
            
            CreateTable(
                "dbo.AppointmentDocuments",
                c => new
                    {
                        AppointmentDocumentId = c.Int(nullable: false, identity: true),
                        AppointmentTypeId = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.AppointmentDocumentId)
                .ForeignKey("dbo.AppointmentTypes", t => t.AppointmentTypeId, cascadeDelete: true)
                .Index(t => t.AppointmentTypeId);
            
            CreateTable(
                "dbo.AppointmentTypes",
                c => new
                    {
                        AppointmentTypeId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.AppointmentTypeId);
            
            CreateTable(
                "dbo.Appointments",
                c => new
                    {
                        AppointmentId = c.Int(nullable: false, identity: true),
                        AppointmentTypeId = c.Int(nullable: false),
                        CustomerId = c.Int(nullable: false),
                        LocationId = c.Int(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                        Notes = c.String(),
                    })
                .PrimaryKey(t => t.AppointmentId)
                .ForeignKey("dbo.AppointmentTypes", t => t.AppointmentTypeId, cascadeDelete: true)
                .ForeignKey("dbo.Customers", t => t.CustomerId, cascadeDelete: true)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .Index(t => t.AppointmentTypeId)
                .Index(t => t.CustomerId)
                .Index(t => t.LocationId);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        CustomerId = c.Int(nullable: false, identity: true),
                        IsCurrentCustomer = c.Boolean(nullable: false),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Phone = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.CustomerId);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        LocationId = c.Int(nullable: false, identity: true),
                        AddressId = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.LocationId)
                .ForeignKey("dbo.Addresses", t => t.AddressId, cascadeDelete: true)
                .Index(t => t.AddressId);
            
            CreateTable(
                "dbo.EmployeeContacts",
                c => new
                    {
                        EmployeeContactId = c.Int(nullable: false, identity: true),
                        LocationId = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        Email = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.EmployeeContactId)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .Index(t => t.LocationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EmployeeContacts", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.Appointments", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.Locations", "AddressId", "dbo.Addresses");
            DropForeignKey("dbo.Appointments", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.Appointments", "AppointmentTypeId", "dbo.AppointmentTypes");
            DropForeignKey("dbo.AppointmentDocuments", "AppointmentTypeId", "dbo.AppointmentTypes");
            DropIndex("dbo.EmployeeContacts", new[] { "LocationId" });
            DropIndex("dbo.Locations", new[] { "AddressId" });
            DropIndex("dbo.Appointments", new[] { "LocationId" });
            DropIndex("dbo.Appointments", new[] { "CustomerId" });
            DropIndex("dbo.Appointments", new[] { "AppointmentTypeId" });
            DropIndex("dbo.AppointmentDocuments", new[] { "AppointmentTypeId" });
            DropTable("dbo.EmployeeContacts");
            DropTable("dbo.Locations");
            DropTable("dbo.Customers");
            DropTable("dbo.Appointments");
            DropTable("dbo.AppointmentTypes");
            DropTable("dbo.AppointmentDocuments");
            DropTable("dbo.Addresses");
        }
    }
}
