﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppointmentScheduler.Models
{
  public class AppointmentType
  {
    
    public int AppointmentTypeId { get; set; }

    [Required]
    public string Name { get; set; }
  }
}