﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppointmentScheduler.Models
{
  public class Location
  {
    public int LocationId { get; set; }

    public int AddressId { get; set; }

    [Required]
    public string Name { get; set; }

    public virtual Address Address { get; set; }
  }
}